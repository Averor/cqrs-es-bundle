<?php declare(strict_types=1);

namespace Averor\CqrsBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 *
 * @package Averor\CqrsBundle\DependencyInjection
 * @author Averor <averor.dev@gmail.com>
 */
class Configuration implements ConfigurationInterface
{
    /**
     * Generates the configuration tree builder.
     *
     * @return \Symfony\Component\Config\Definition\Builder\TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();

        $rootNode = $treeBuilder->root('cqrs');

        $rootNode
            ->children()

                // message_bus
                ->arrayNode('message_bus')->isRequired()
                    ->children()

                        // command_bus
                        ->arrayNode('command_bus')
                            ->append($this->appendLoggingSection())
                        ->end()

                        // event_bus
                        ->arrayNode('event_bus')
                            ->append($this->appendLoggingSection())
                        ->end()

                        // query_bus
                        ->arrayNode('query_bus')
                            ->append($this->appendLoggingSection(null, 'debug'))
                        ->end()

                    ->end()
                ->end() // message_bus

            ->end()
        ;

        return $treeBuilder;
    }

    protected function appendLoggingSection(?string $serviceId = 'logger', ?string $level = 'info') : NodeDefinition
    {
        $treeBuilder = new TreeBuilder();
        $node = $treeBuilder->root('logging');

        $serviceId = $serviceId ?: 'logger';
        $level = $level ?: 'info';

        $node
            ->children()
                ->scalarNode('logger')->cannotBeEmpty()->defaultValue($serviceId)->end()
                ->scalarNode('level')->cannotBeEmpty()->defaultValue($level)->end()
            ->end();

        return $node;
    }
}
