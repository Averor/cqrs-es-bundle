<?php declare(strict_types=1);

namespace Averor\CqrsBundle\DependencyInjection;

use Averor\MessageBus\Contract\CommandHandler;
use Averor\MessageBus\Contract\EventListener;
use Averor\MessageBus\Contract\ProcessManager;
use Averor\MessageBus\CommandBus;
use Averor\MessageBus\EventBus;
use Averor\MessageBus\EventSourcing\Contract\Projector;
use Averor\MessageBus\QueryBus;
use Averor\MessageBus\QueryBus\Contract\QueryHandler;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\ConfigurableExtension;

/**
 * Class CqrsExtension
 *
 * @package Averor\CqrsBundle\DependencyInjection
 * @author Averor <averor.dev@gmail.com>
 */
class CqrsExtension extends ConfigurableExtension
{
    public function getAlias()
    {
        return 'cqrs';
    }

    /**
     * @param array $config
     * @param ContainerBuilder $container
     * @return Configuration|null|object|\Symfony\Component\Config\Definition\ConfigurationInterface
     */
    public function getConfiguration(array $config, ContainerBuilder $container)
    {
        return new Configuration();
    }

    /**
     * Configures the passed container according to the merged configuration.
     *
     * @param array $mergedConfig
     * @param ContainerBuilder $container
     * @throws \Exception
     */
    protected function loadInternal(array $mergedConfig, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(dirname(__DIR__).'/Resources/config')
        );

        $loader->load('services.yaml');

        if (array_key_exists('command_bus', $mergedConfig['message_bus'] ?? [])) {

            if (array_key_exists('logging', $mergedConfig['message_bus']['command_bus'])) {

                $logger = $mergedConfig['message_bus']['command_bus']['logging']['logger'];
                $logLevel = $mergedConfig['message_bus']['command_bus']['logging']['level'];

                $container->getDefinition('Averor.CommandBus.Logger')
                    ->setArgument('$logger', new Reference($logger))
                    ->setArgument('$level', $logLevel);

                $container->getDefinition(CommandBus::class)
                    ->addMethodCall(
                        'prependMiddleware',
                        [$container->getDefinition('Averor.CommandBus.Logger')]
                    );
            } else {
                $container->removeDefinition('Averor.CommandBus.Logger');
            }

        } else {
            $container->removeDefinition(CommandBus::class);
            $container->removeDefinition('Averor.CommandBus.Resolver');
            $container->removeDefinition('Averor.CommandBus.Dispatcher');
            $container->removeDefinition('Averor.CommandBus.Logger');
        }

        if ($mergedConfig['message_bus']['event_bus'] ?? false) {

            $logger = $mergedConfig['message_bus']['event_bus']['logging']['logger'];
            $logLevel = $mergedConfig['message_bus']['event_bus']['logging']['level'];

            $container->getDefinition('Averor.EventBus.Logger')
                ->setArgument('$logger', new Reference($logger))
                ->setArgument('$level', $logLevel);

            $container->getDefinition(EventBus::class)
                ->addMethodCall(
                    'prependMiddleware',
                    [$container->getDefinition('Averor.EventBus.Logger')]
                );
        } else {
            $container->removeDefinition(EventBus::class);
            $container->removeDefinition('Averor.EventBus.Resolver');
            $container->removeDefinition('Averor.EventBus.Dispatcher');
            $container->removeDefinition('Averor.EventBus.Logger');
        }

        if ($mergedConfig['message_bus']['query_bus'] ?? false) {

            $logger = $mergedConfig['message_bus']['query_bus']['logging']['logger'];
            $logLevel = $mergedConfig['message_bus']['query_bus']['logging']['level'];

            $container->getDefinition('Averor.QueryBus.Logger')
                ->setArgument('$logger', new Reference($logger))
                ->setArgument('$level', $logLevel);

            $container->getDefinition(QueryBus::class)
                ->addMethodCall(
                    'prependMiddleware',
                    [$container->getDefinition('Averor.QueryBus.Logger')]
                );
        } else {
            $container->removeDefinition(QueryBus::class);
            $container->removeDefinition('Averor.QueryBus.Resolver');
            $container->removeDefinition('Averor.QueryBus.Dispatcher');
            $container->removeDefinition('Averor.QueryBus.Logger');
        }

        $container->registerForAutoconfiguration(CommandHandler::class)
            ->addTag('averor.message_bus.command_handler');

        $container->registerForAutoconfiguration(EventListener::class)
            ->addTag('averor.message_bus.event_listener');

        $container->registerForAutoconfiguration(ProcessManager::class)
            ->addTag('averor.message_bus.process_manager');

        $container->registerForAutoconfiguration(Projector::class)
            ->addTag('averor.message_bus.projector');

        $container->registerForAutoconfiguration(QueryHandler::class)
            ->addTag('averor.message_bus.query_handler');
    }
}
