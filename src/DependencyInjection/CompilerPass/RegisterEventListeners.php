<?php declare(strict_types=1);

namespace Averor\CqrsBundle\DependencyInjection\CompilerPass;

use Averor\MessageBus\Contract\Event;
use ReflectionClass;
use ReflectionParameter;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class RegisterEventListeners
 *
 * @package Averor\CqrsBundle\DependencyInjection\CompilerPass
 * @author Averor <averor.dev@gmail.com>
 */
class RegisterEventListeners implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     * @return void
     * @throws \ReflectionException
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('Averor\MessageBus\EventBus')) {
            return;
        }

        if (!$container->has('Averor.EventBus.Resolver')) {
            return;
        }

        $resolver = $container->findDefinition('Averor.EventBus.Resolver');

        $mapArgument = $resolver->getArgument('$map');

        $handlersIds = $container->findTaggedServiceIds('averor.message_bus.event_listener');

        foreach ($handlersIds as $handlerId => $tags) {

            $container->getDefinition($handlerId)->setLazy(true);
            
            $mapArgument = array_merge_recursive(
                $mapArgument,
                static::buildMap($handlerId)
            );
        }

        $resolver->setArgument('$map', $mapArgument);
    }

    /**
     * @param string $handlerId
     * @return array
     * @throws \ReflectionException
     */
    protected static function buildMap(string $handlerId) : array
    {
        $reflection = new ReflectionClass($handlerId);

        $result = [];

        /** @var \ReflectionMethod $method */
        foreach($reflection->getMethods() as $method) {

            if (
                $method->getName() !== "__invoke"
                && substr($method->getName(), 0, 2) !== 'on'
            ) {
                continue;
            }

            $params = $method->getParameters();

            if (count($params) !== 1) {
                continue;
            }

            /** @var ReflectionParameter $param */
            $param = $params[0];

            /** @var ReflectionClass|null $paramClass */
            $paramClass = $param->getClass();

            if ($paramClass === null || !$paramClass->implementsInterface(Event::class)) {
                continue;
            }

            if ($method->getName() === "__invoke") {
                $result[$paramClass->getName()][] = new Reference($handlerId);
            } else {
                $result[$paramClass->getName()][] = [new Reference($handlerId), $method->getName()];
            }
        }

        return $result;
    }
}
