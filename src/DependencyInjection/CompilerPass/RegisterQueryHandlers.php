<?php declare(strict_types=1);

namespace Averor\CqrsBundle\DependencyInjection\CompilerPass;

use Averor\MessageBus\QueryBus\Contract\Query;
use ReflectionClass;
use ReflectionParameter;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class RegisterQueryHandlers
 *
 * @package Averor\CqrsBundle\DependencyInjection\CompilerPass
 * @author Averor <averor.dev@gmail.com>
 */
class RegisterQueryHandlers implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     * @return void
     * @throws \ReflectionException
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('Averor\MessageBus\QueryBus')) {
            return;
        }

        if (!$container->has('Averor.QueryBus.Resolver')) {
            return;
        }

        $resolver = $container->findDefinition('Averor.QueryBus.Resolver');

        $mapArgument = $resolver->getArgument('$map');

        $handlersIds = $container->findTaggedServiceIds('averor.message_bus.query_handler');

        foreach ($handlersIds as $handlerId => $tags) {

            $container->getDefinition($handlerId)->setLazy(true);
            
            $mapArgument = array_merge(
                $mapArgument,
                static::buildMap($handlerId)
            );
        }

        $resolver->setArgument('$map', $mapArgument);
    }

    /**
     * @param string $handlerId
     * @return array
     * @throws \ReflectionException
     */
    protected static function buildMap(string $handlerId) : array
    {
        $reflection = new ReflectionClass($handlerId);

        $result = [];

        /** @var \ReflectionMethod $method */
        foreach($reflection->getMethods() as $method) {

            if (
                $method->getName() !== "__invoke"
                && substr($method->getName(), 0, 6) !== 'handle'
            ) {
                continue;
            }

            $params = $method->getParameters();

            if (count($params) !== 1) {
                continue;
            }

            /** @var ReflectionParameter $param */
            $param = $params[0];

            /** @var ReflectionClass|null $paramClass */
            $paramClass = $param->getClass();

            if ($paramClass === null || !$paramClass->implementsInterface(Query::class)) {
                continue;
            }

            if ($method->getName() === "__invoke") {
                $result[$paramClass->getName()] = new Reference($handlerId);
            } else {
                $result[$paramClass->getName()] = [new Reference($handlerId), $method->getName()];
            }
        }

        return $result;
    }
}
