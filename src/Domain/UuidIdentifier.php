<?php declare(strict_types=1);

namespace Averor\CqrsBundle\Domain;

use Averor\MessageBus\Contract\Identifier;
use Ramsey\Uuid\Uuid;

/**
 * Class UuidIdentifier
 *
 * @package Averor\CqrsBundle\Domain
 * @author Averor <averor.dev@gmail.com>
 */
class UuidIdentifier implements Identifier
{
    /** @var string */
    protected $uuid;

    public function __construct(?string $from = null)
    {
        $uuid = $from
            ? Uuid::fromString($from)->toString()
            : Uuid::uuid4()->toString();

        $this->uuid = strtolower($uuid);
    }

    public static function create() : Identifier
    {
        return new static();
    }

    public static function fromString(string $identifier) : Identifier
    {
        return new static($identifier);
    }

    public function __toString() : string
    {
        return $this->toString();
    }

    public function toString() : string
    {
        return $this->uuid;
    }

    /**
     * @param string $string
     * @return bool
     */
    public static function isValid(string $string) : bool
    {
        return Uuid::isValid($string);
    }
}
