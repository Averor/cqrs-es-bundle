<?php declare(strict_types=1);

namespace Averor\CqrsBundle\Domain;

use DateTimeImmutable;
use DateTimeInterface;
use Ramsey\Uuid\Uuid;

/**
 * Trait DomainEventTrait
 *
 * @package Averor\CqrsBundle\Domain
 * @author Averor <averor.dev@gmail.com>
 */
trait DomainEventTrait
{
    /** @var string Message ID */
    protected $_id;

    /** @var DateTimeInterface Message create date */
    protected $_date;

    /**
     * @throws \Exception
     */
    public function __construct()
    {
        $this->_id = Uuid::uuid4()->toString();
        $this->_date = new DateTimeImmutable();
    }

    public function _id() : string
    {
        return $this->_id;
    }

    public function _date() : DateTimeInterface
    {
        return $this->_date;
    }
}
