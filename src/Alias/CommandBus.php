<?php declare(strict_types=1);

namespace Averor\MessageBus;

/**
 * Class CommandBus
 *
 * @package Averor\MessageBus
 * @author Averor <averor.dev@gmail.com>
 */
class CommandBus extends MessageBus
{
}
