<?php declare(strict_types=1);

namespace Averor\MessageBus;

/**
 * Class QueryBus
 *
 * @package Averor\MessageBus
 * @author Averor <averor.dev@gmail.com>
 */
class QueryBus extends \Averor\MessageBus\QueryBus\QueryBus
{
}
