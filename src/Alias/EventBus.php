<?php declare(strict_types=1);

namespace Averor\MessageBus;

use Averor\MessageBus\EventSourcing\Contract\EventBus as IEventBus;

/**
 * Class EventBus
 *
 * @package Averor\MessageBus
 * @author Averor <averor.dev@gmail.com>
 */
class EventBus extends MessageBus implements IEventBus
{
}
