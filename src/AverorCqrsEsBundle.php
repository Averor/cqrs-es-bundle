<?php declare(strict_types=1);

namespace Averor\CqrsBundle;

use Averor\CqrsBundle\DependencyInjection\CompilerPass\RegisterCommandHandlers;
use Averor\CqrsBundle\DependencyInjection\CompilerPass\RegisterEventListeners;
use Averor\CqrsBundle\DependencyInjection\CompilerPass\RegisterProcessManagers;
use Averor\CqrsBundle\DependencyInjection\CompilerPass\RegisterProjectors;
use Averor\CqrsBundle\DependencyInjection\CompilerPass\RegisterQueryHandlers;
use Averor\CqrsBundle\DependencyInjection\CqrsExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class AverorCqrsEsBundle
 *
 * @package Averor\CqrsBundle
 * @author Averor <averor.dev@gmail.com>
 */
class AverorCqrsEsBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(
            new RegisterCommandHandlers()
        );

        $container->addCompilerPass(
            new RegisterEventListeners()
        );

        $container->addCompilerPass(
            new RegisterProjectors()
        );

        $container->addCompilerPass(
            new RegisterProcessManagers()
        );

        $container->addCompilerPass(
            new RegisterQueryHandlers()
        );
    }

    public function getContainerExtension()
    {
        return new CqrsExtension();
    }
}
