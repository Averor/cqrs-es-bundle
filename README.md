# Averor/Cqrs es Bundle

...description goes here...

## Install using Composer:

`composer require averor/cqrs-es-bundle`

## Enable bundle in Symfony
 
Tag handlers:

> averor.message_bus.command_handler

> averor.message_bus.event_listener

> averor.message_bus.query_handler

or use _Interface-based service configuration_:

```yaml
_instanceof:
    Averor\MessageBus\Contract\CommandHandler:
        tags: ['averor.message_bus.command_handler']
    Averor\MessageBus\Contract\EventListener:
        tags: ['averor.message_bus.event_listener']
    Averor\MessageBus\QueryBus\Contract\QueryHandler:
        tags: ['averor.message_bus.query_handler']
```
